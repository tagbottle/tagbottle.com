import encodeThemesToTagString from "tomato-puree/util/convert/tag/encodeThemesToTagString";
import { firebaseAction } from "vuexfire";
import * as firebase from "firebase";
import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IBindingUserState, IBindUserData, IState } from "~/store";

/*
 * state
 */

export const state = (): IBindingUserState => {
  return {};
};

/*
 * getters
 */

export const getters: GetterTree<IBindingUserState, IState> = {
  data(state): (userId: string) => IBindUserData | null {
    return userId => {
      return state[userId] as IBindUserData;
    };
  }
};

/*
 * actions
 */

export const actions: ActionTree<IBindingUserState, IState> = {
  bind: firebaseAction(
    async (
      { bindFirebaseRef, state, commit },
      props: { userRef: firebase.firestore.DocumentReference }
    ) => {
      const { userRef } = props;
      const key = userRef.id;
      if (!key || state[key]) {
        return;
      }

      commit("ADD_OBJECT", { key });
      await bindFirebaseRef(key, userRef, { maxRefDepth: 0 });
    }
  )
};

/*
 * mutations
 */

export const mutations: MutationTree<IBindingUserState> = {
  ADD_OBJECT: (state, props: { key: string }) => {
    Vue.set(state, props.key, {});
  }
};
