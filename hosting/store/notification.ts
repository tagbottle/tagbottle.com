import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { INotificationState, INotificationObject, IState } from "./index";
import { RefGenerator } from "tomato-puree/client/ref";
import DocTypes from "tomato-puree/client/firestoreDocType";

type IModuleState = INotificationState;

/*
    state
 */

export const state = (): IModuleState => {
  return {
    notifications: []
  };
};

/*
    actions
 */

export const actions: ActionTree<IModuleState, IState> = {
  async requestPermission() {
    // create ref
    const db = this.$firebase.firestore();
    const refGenerator = new RefGenerator(db);
    const userId = this.$firebase.auth().currentUser.uid;
    const userDeviceRef = refGenerator.userDevice(userId);

    // request permission
    const messaging = this.$firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();

    // save token
    const userDeviceDoc: DocTypes.IUserDeviceDocument = {
      devices: [
        {
          active: true,
          name: "test device",
          token
        }
      ]
    };
    await userDeviceRef.set(userDeviceDoc, { merge: true });
  },

  add({ commit }, props: { notification: INotificationObject }) {
    commit("SAVE_NOTIFICATIONS", props.notification);
  }
};

/*
    mutations
 */

export const mutations: MutationTree<IModuleState> = {
  SAVE_NOTIFICATIONS(state, payload: INotificationObject) {
    state.notifications.unshift(payload);
  }
};
