import * as clientPuree from "tomato-puree/client";
import isInvalidThemes from "tomato-puree/util/validate/tag/isInvalidThemes";
import themesForTag from "tomato-puree/util/convert/tag/themesForTag";
import encodeThemesToTagString from "tomato-puree/util/convert/tag/encodeThemesToTagString";
import RefGenerator = clientPuree.Ref.RefGenerator;

import { firebaseAction } from "vuexfire";
import * as firebase from "firebase/app";
import Vue from "vue";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import { IState, IChatState, IBindTagData, IBindMessageData } from "./index";
import { TagRef } from "tomato-puree/client/ref/pureeRef";
import { IStampDocument } from "tomato-puree/client/firestoreDocType";
import { flatten, uniqBy, sortedUniqBy } from "lodash";

// sort desc order with timestamp
// if timestamp is null, treat them as latest messages
function sortMessages(messages: IMessageData[]): IMessageData[] {
  return messages.sort((a, b) => {
    const left = a.timestamp;
    const right = b.timestamp;
    if (left && right) {
      return right.seconds - left.seconds;
    } else if (left && !right) {
      return 1;
    } else if (!left && right) {
      return -1;
    } else {
      return 0;
    }
  });
}
function citeMessages(text: string, messages: IMessageData[]): string[] {
  // parse text and extract citation numbers
  const pattern = /@\d{1,3}/g;
  const citeStrings = text.match(pattern);
  const citeIndexes = citeStrings
    ? citeStrings.map(x => parseInt(x.slice(1)))
    : [];

  // get message objects from message numbers
  const citeMessages = messages.filter(
    message => citeIndexes.indexOf(message.index) > -1
  );

  // return array of message ids
  return citeMessages.length > 0 ? citeMessages.map(message => message.id) : [];
}

/*
 * state
 */

export const state = (): IChatState => ({
  themes: [],
  recentTags: [],
  loading: true,
  currentIndex: null,
  bottleIndex: null,
  bottlePath: null,
  recentBottle: []
});

/*
 * getters
 */

export interface IMessageData extends IBindMessageData {
  loaded: boolean;
}
export interface IBottlesData {
  index: number;
  bottlePath: string;
}

export const getters: GetterTree<IChatState, IState> = {
  // messages for center column
  messages(state, getters): IMessageData[] {
    if (state.loading) {
      return [];
    }
    return sortedUniqBy(sortMessages(getters["allMessages"][0]), "id");
  },

  // messages for right column
  subMessages(state, getters): IMessageData[] {
    if (state.loading) {
      return [];
    }
    return sortedUniqBy(
      sortMessages(flatten(getters["allMessages"].slice(1))),
      "id"
    );
  },

  // all binded messages
  allMessages(state, getters, rootState, rootGetters): IMessageData[][] {
    const result: IMessageData[][] = [];
    for (const bottle of getters["recentBottle"]) {
      const bindMessages: IBindMessageData[] =
        rootGetters["binding/bottleMessages/data"](bottle.bottlePath) || [];

      const bottleResult: IMessageData[] = [];
      for (const bindMessage of bindMessages) {
        let loaded = true;

        const user = rootGetters["binding/user/data"](bindMessage.userId);

        // check user binding
        loaded = loaded && !!user;

        // check timestamp by server
        // if timestamp is null, this message is not stored in server-side
        loaded = loaded && !!bindMessage.timestamp;

        const message: IMessageData = Object.assign({}, bindMessage, {
          user,
          loaded
        });
        bottleResult.unshift(message);
      }
      result.push(bottleResult);
    }

    return result;
  },

  bottles(state, getters, rootState, rootGetters): IBottlesData[] {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;

    const result: IBottlesData[] = [];
    if (tag && tag.bottles) {
      const keys = Object.keys(tag.bottles).map(i => parseInt(i));
      keys.sort((a, b) => a - b);
      for (const key of keys) {
        const index = +key;
        const bottlePath = tag.bottles[key];
        result.push({ index, bottlePath });
      }
    }

    // desc order
    return result.reverse();
  },

  index(state, getters): number {
    // TODO: this should be done by firestore callback?
    const messages = getters["messages"];
    const messagesCount = messages.length;
    const biggestIndex = Math.max(...messages.map(m => m.index));
    return Math.max(messagesCount, biggestIndex);
  },

  currentBottleIndex(state, getters, rootState, rootGetters): number | null {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;
    if (tag) {
      return tag.currentIndex;
    } else {
      return state.bottleIndex;
    }
  },
  isLatestBottle(state, getters, rootState, rootGetters): boolean {
    const tag = rootGetters["binding/tag/data"](state.themes) as IBindTagData;
    if (tag) {
      return state.bottleIndex === tag.currentIndex;
    } else {
      return true;
    }
  },

  stamps(state) {
    return state[`stamps-${state.bottlePath}`] || {};
  },

  hasOlderMessagesOfCurrentBottle(
    state,
    getters,
    rootState,
    rootGetters
  ): boolean {
    return !rootGetters["binding/bottleMessages/isAllBind"](
      state.bottlePath
    ) as boolean;
  },

  recentBottle(state) {
    if (state.recentBottle.length > 5) {
      // TODO: unbind unnecessary binding
      return state.recentBottle.slice(0, 5);
    } else {
      return state.recentBottle;
    }
  }
};

export const actions: ActionTree<IChatState, IState> = {
  async selectThemes({ state, commit, dispatch }, { themes, index }) {
    // replace selected tag if given one is invalid
    if (!isInvalidThemes(themes)) {
      commit("SELECT_THEMES", { themes });
    } else if (themes.length === 0) {
      // or use default value, if empty themes selected
      commit("SELECT_THEMES", { themes: ["hello", "world"] });
    }

    await dispatch("tag/addTag", { themes: state.themes }, { root: true });

    await dispatch("startChatByThemes", { themes: state.themes, index });
  },

  async sendMessage({ state, getters }, { themes, text }) {
    // message needs bottle param
    if (!state.bottlePath) {
      return;
    }

    // get ids of citation
    const citations = citeMessages(text, getters["messages"]);

    //  create message
    const db = this.$firebase.firestore();
    const user = this.$firebase
      .firestore()
      .collection("/users")
      .doc(this.$firebase.auth().currentUser.uid);
    const stamps = [];
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    const bottle = db.doc(state.bottlePath);
    const index = getters["index"] + 1;
    await this.$firebase
      .firestore()
      .collection("/messages")
      .add({ user, index, stamps, text, themes, timestamp, bottle, citations });

    // rebottle (if needed)
    if (index >= 200) {
      // TODO: remove this dirty workaround...
      const bottleId = state.bottlePath.split("/")[1];
      await this.$firebase.functions().httpsCallable("chat-rebottle")({
        bottleId
      });
    }
  },

  async editMessage({ state, getters }, { messageId, text }) {
    // get ids of citation
    const citations = citeMessages(text, getters["messages"]);

    // update firestore
    await this.$firebase
      .firestore()
      .collection("/messages")
      .doc(messageId)
      .update({ text, citations });
  },

  async sendStamp(
    { state },
    props: { messageId: string; stampSetId: string; stampItemId: string }
  ) {
    if (!state.bottlePath) {
      return;
    }
    const { messageId, stampSetId, stampItemId } = props;

    // create refs
    const db = this.$firebase.firestore();
    const refGenerator = new RefGenerator(db);

    const userRef = refGenerator.user(this.$firebase.auth().currentUser.uid);
    const stampSetRef = refGenerator.stampSet(stampSetId);
    const messageDocRef = db.collection("messages").doc(messageId);
    const newStampRef = refGenerator.newStamp();

    if (!userRef) {
      return;
    }

    // use stamp
    const data: IStampDocument = {
      user: userRef.ref,
      stampSet: stampSetRef.ref,
      message: messageDocRef,
      stampItemId,
      status: "pending"
    };
    await newStampRef.set(data);
  },

  async startChatByThemes(
    { commit, state, dispatch, getters, rootGetters },
    {
      attempt = 0,
      themes,
      index = null
    }: { attempt: 0 | 1 | 2 | 3; themes: string[]; index: number | null }
  ) {
    // return if `themes` is null
    if (!themes) {
      console.error("startChatByThemes null themes");
      return;
    }

    // check auth status
    if (!this.$firebase.auth().currentUser) {
      console.error("startChatByThemes failed: currentUser is blank");
      return;
    }

    // check attempt count
    if (attempt > 30) {
      throw new Error("startChatByThemes: attempt 4");
    }

    // set up ref
    const db = this.$firebase.firestore();
    const refManager = new RefGenerator(db);
    const tagRef = refManager.tag(themes) as TagRef;

    // create tag document if it does not exist yet
    // check only at first time
    const tag = rootGetters["binding/tag/data"](themes) as IBindTagData;
    const invalidTag = !tag || !tag.currentBottle;
    if (attempt == 0 && invalidTag) {
      // start load
      commit("LOADING_STATUS", { loading: true });

      // create tag document if it has not been created
      const tagExists = (await tagRef.ref.get()).exists;
      if (!tagExists) {
        // create
        const createTagFunction = this.$firebase
          .functions()
          .httpsCallable("chat-createTag");
        await createTagFunction({ themes });
      }

      // bind tag
      await dispatch(
        "binding/tag/bind",
        { themes, tagRef: tagRef.ref },
        { root: true }
      );

      // dispatch next try with promise, to manage all chat-stat sequence in one thread
      // in other words, this `setTimeout` is used to wait for binding of firestore document, not to start new thread
      return new Promise(resolve => {
        setTimeout(async () => {
          const nextResult = await dispatch("startChatByThemes", {
            themes,
            index,
            attempt: 1
          });
          return resolve(nextResult);
        }, 100);
      });
    }

    // wait until tag-bind
    if (invalidTag) {
      console.warn("startChatByThemes tag:bind:retry");
      return new Promise(resolve => {
        setTimeout(async () => {
          const nextResult = await dispatch("startChatByThemes", {
            themes,
            index,
            attempt: attempt + 1
          });
          return resolve(nextResult);
        }, 100);
      });
    }

    // select index
    const bottleIndex: number = index ? index : tag.currentIndex;
    const bottlePath: string =
      tag.currentIndex == bottleIndex
        ? tag.currentBottle
        : tag.bottles[bottleIndex];

    // set message query
    const bottleRef = db.doc(bottlePath);
    await dispatch(
      "binding/bottleMessages/bind",
      { bottleRef },
      { root: true }
    );

    // save bottle info
    commit("SAVE_BOTTLE_INFO", { bottlePath, bottleIndex });
    commit("SAVE_RECENT_BOTTLE_INFO", {
      themes,
      bottlePath,
      bottleIndex,
      currentIndex: tag.currentIndex
    });

    // end load
    commit("LOADING_STATUS", { loading: false });
  },

  async selectBottleIndex(
    { commit, dispatch, getters, state, rootGetters },
    { index }
  ) {
    const tag: IBindTagData | null = rootGetters["binding/tag/data"](
      state.themes
    );
    if (!tag || !tag.bottles) {
      console.warn("selectBottleIndex failed");
      return;
    }

    const bottlePath = tag.bottles[index];

    // set message query
    const db = this.$firebase.firestore();
    const bottleRef = db.doc(bottlePath);
    await dispatch(
      "binding/bottleMessages/bind",
      { bottleRef },
      { root: true }
    );

    commit("SAVE_BOTTLE_INFO", { bottlePath, bottleIndex: index });
    commit("SAVE_RECENT_BOTTLE_INFO", {
      themes: state.themes,
      bottlePath,
      bottleIndex: index,
      currentIndex: state.currentIndex
    });
  },

  async loadAllMessagesOfCurrentBottle({ state, dispatch }) {
    const db = this.$firebase.firestore();

    const bottleRef = db.doc(state.bottlePath);

    await dispatch(
      "binding/bottleMessages/bindAll",
      { bottleRef },
      { root: true }
    );
  }
};

export const mutations: MutationTree<IChatState> = {
  SELECT_THEMES(state, { themes }) {
    state.themes = themes;
  },
  SAVE_BOTTLE_INFO(state, { bottlePath, bottleIndex }) {
    state.bottlePath = bottlePath;
    state.bottleIndex = bottleIndex;
  },
  SAVE_RECENT_BOTTLE_INFO(
    state,
    { themes, bottlePath, bottleIndex, currentIndex }
  ) {
    if (!themes) {
      return;
    }
    const tag = encodeThemesToTagString(themes) as string;
    state.recentBottle.unshift({
      tag,
      themes,
      bottlePath,
      bottleIndex,
      currentIndex
    });
    state.recentBottle = uniqBy(state.recentBottle, "tag");
  },
  LOADING_STATUS(state, props: { loading: boolean }) {
    state.loading = props.loading;
  }
};
