import Vue from "vue";
import { GetterTree, ActionTree, MutationTree } from "vuex";
import { RefGenerator } from "tomato-puree/client/ref";
import { IStampSetInfo, IStampState, IState } from "~/store";

type IModuleState = IStampState;

/*
 * state
 */

export const state = (): IModuleState => {
  return {
    tempStampSets: []
  };
};

/*
   getters
 */

function createStampSetList(
  targets: Array<{ stampSet: string; amount: number }>,
  rootState: IState
): IStampSetInfo[] {
  const loadedStampSets = rootState.binding.stampSet;
  const result: IStampSetInfo[] = [];

  for (const stockTarget of targets) {
    // check loaded data
    // (pass if it's not loaded yet)
    const stampSetDoc = loadedStampSets[stockTarget.stampSet];
    if (!stampSetDoc || !stampSetDoc.items) {
      continue;
    }

    const doc: IStampSetInfo = {
      ...stampSetDoc,
      stampSetId: stockTarget.stampSet
    };
    result.push(doc);
  }

  return result;
}

export const getters: GetterTree<IModuleState, IState> = {
  favorites(state, getters, rootState, rootGetters): IStampSetInfo[] {
    // return empty array if stamp-stock is not bind yet
    if (!rootState.binding || !rootState.binding.stampStock) {
      return [];
    }

    return createStampSetList(
      rootState.binding.stampStock.favorites,
      rootState
    );
  },
  latestBought(state, getters, rootState, rootGetters): IStampSetInfo[] {
    // return empty array if stamp-stock is not bind yet
    if (!rootState.binding || !rootState.binding.stampStock) {
      return [];
    }

    return createStampSetList(
      rootState.binding.stampStock.latestBought,
      rootState
    );
  },
  latestUsed(state, getters, rootState, rootGetters): IStampSetInfo[] {
    // return empty array if stamp-stock is not bind yet
    if (!rootState.binding || !rootState.binding.stampStock) {
      return [];
    }

    return createStampSetList(
      rootState.binding.stampStock.latestUsed,
      rootState
    );
  }
};

/*
  actions
*/

export const actions: ActionTree<IModuleState, IState> = {
  async loadTempStampSets({ commit, state }) {
    if (state.tempStampSets.length) {
      return;
    }
    // get stamp sets
    const db = this.$firebase.firestore();
    const refGenerator = new RefGenerator(db);
    const stampSetsData = await refGenerator
      .stampSets({ venderId: "ef" })
      .data();

    const stampSets: IStampSetInfo[] = stampSetsData.list.map(d => {
      return { ...d.data, stampSetId: d.ref.ref.id };
    });
    commit("SAVE_TEMP_STAMP_SETS", { stampSets });
  }
};

/*
  mutations
*/

export const mutations: MutationTree<IModuleState> = {
  SAVE_TEMP_STAMP_SETS(state, value: { stampSets: IStampSetInfo[] }) {
    Vue.set(state, "tempStampSets", value.stampSets);
  }
};
