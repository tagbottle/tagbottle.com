import firebase from "firebase";
import { GetterTree, ActionTree, MutationTree } from "vuex";
import { RefGenerator } from "tomato-puree/client/ref";

import { IAuthState, IState } from "./index";

type ModuleState = IAuthState;

/*
 * state
 */

export const state = (): ModuleState => ({
  authInitialized: false,
  loggedIn: false
});

/*
 * getters
 */

export const getters: GetterTree<ModuleState, IState> = {
  authInitialized: state => state.authInitialized,
  loggedIn: state => state.loggedIn,
  loginRedirectNeeded: (state, getters): boolean => {
    return getters["authInitialized"] && !getters["loggedIn"];
  }
};

/*
 * actions
 */

export const actions: ActionTree<ModuleState, IState> = {
  async login({ state, commit }, props: { user: firebase.User }) {
    commit("LOGIN");
  },

  async logout({ state, commit }) {
    commit("LOGOUT");
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<ModuleState> = {
  LOGIN(state) {
    state.authInitialized = true;
    state.loggedIn = true;
  },
  LOGOUT(state) {
    state.authInitialized = true;
    state.loggedIn = false;
  }
};
