import * as DocTypes from "tomato-puree/client/firestoreDocType";
import { firebaseMutations } from "vuexfire";
import { IStampSetDocument } from "tomato-puree/client/firestoreDocType";

export const mutations = {
  ...firebaseMutations
};

export interface IState {
  app: IAppState;
  auth: IAuthState;
  chat: IChatState;
  stamp: IStampState;
  tag: ITagState;
  notification: INotificationState;
  binding: {
    bottleMessages: IBindingBottleMessagesState;
    stampSet: IBindingStampSetState;
    stampStock: IBindingStampStockState;
    tag: IBindingTagState;
    user: IBindingUserState;
  };
}

/*
  app.ts
 */

export interface IAppState {
  authInitialized: boolean;
  firebaseInitialized: boolean;
  bookmark: any;
  currentUser: any;
  notification: any;
}

/*
  auth.ts
 */

export interface IAuthState {
  authInitialized: boolean;
  loggedIn: boolean;
}

/*
  chat.ts
 */

export interface IChatState {
  themes: string[];
  recentTags: string[];
  loading: boolean;
  bottleIndex: number | null;
  bottlePath: string | null;
  currentIndex: number | null;
  recentBottle: object[];
}

/*
  notification.ts
 */

export interface INotificationState {
  notifications: INotificationObject[];
}

export interface INotificationObject {
  encodedTag: string;
  iconUrl: string;
  pingDisplayName: string;
  pingUsername: string;
}

/*
  stamp.ts
 */
export interface IStampState {
  tempStampSets: IStampSetInfo[];
}
export interface IStampSetInfo extends IStampSetDocument {
  stampSetId: string;
}

/*
  tag.ts
 */

export interface ITagState {}

/*
  binding/bottleMessages.ts
 */

export interface IBindingBottleMessagesState {
  [key: string]: IBindMessageData[] | null;
}
export interface IBindMessageData {
  id: string;
  index: number;
  text: string;
  timestamp: DocTypes.CommonType.ITimestamp;
  stamps: object;
  themes: string[];
  userId: string;
  citations: string[];
}

/*
  binding/stampSet.ts
 */

export interface IBindingStampSetState {
  [key: string]: DocTypes.IStampSetDocument | null;
}

/*
  binding/stampStock.ts
 */
export interface IBindingStampStockState extends DocTypes.IStampStock {
  favorites: Array<{ stampSet: string; amount: number }>;
  latestUsed: Array<{ stampSet: string; amount: number }>;
  latestBought: Array<{ stampSet: string; amount: number }>;
}

/*
  binding/tag.ts
 */

export interface IBindingTagState {
  [key: string]: IBindTagData | null;
}
export interface IBindTagData extends DocTypes.ITag {
  currentBottle: string;
  currentIndex: number;
  bottles: { [key: number]: string };
}

/*
  binding/user.ts
 */

export interface IBindingUserState {
  [key: string]: IBindUserData | null;
}
export interface IBindUserData extends DocTypes.IUserDocument {}
