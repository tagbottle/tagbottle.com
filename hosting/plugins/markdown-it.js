import MarkdownIt from "markdown-it";
import MarkdownItEmoji from "markdown-it-emoji";
import hljs from "highlight.js";
import { tagRule, tagRender } from "./markdown-it-tag";
import { videoRule, videoRender } from "./markdown-it-video";
import { emojis } from "~/static/emojis";

function convertCode(lang, str, escapeHandler) {
  try {
    // convert as requested
    if (lang && hljs.getLanguage(lang)) {
      return hljs.highlight(lang, str).value;
    }
    // simple escape if requested language is not supported
    if (lang) {
      return escapeHandler(str);
    }
    // auto convert if no request
    return hljs.highlightAuto(str).value;
  } catch (err) {
    // simple escape if error happens
    return escapeHandler(str);
  }
}

export default async ({ store }, inject) => {
  // init
  const md = new MarkdownIt({
    html: false,
    breaks: true,
    linkify: true,
    linkTarget: "_blank",
    highlight: function(str, syntax) {
      const [lang, ...others] = syntax.split(":");
      const filename = md.utils.escapeHtml(others.join(":"));
      const code = convertCode(lang, str, md.utils.escapeHtml);
      const langClass = lang ? `language-${lang}` : "";
      return filename
        ? `<pre class="hljs ${langClass}" data-name="${filename}"><code>${code}</code></pre>`
        : `<pre class="hljs ${langClass}"><code>${code}</code></pre>`;
    },
    typographer: false
  });

  // configure rules to use
  md.configure({
    components: {
      core: {
        rules: [
          "block",
          "inline",
          "linkify",
          "normalize",
          "replacements",
          "smartquotes"
        ]
      },
      block: {
        rules: [
          "blockquote",
          "code",
          "fence",
          "heading",
          "hr",
          "html_block",
          "lheading",
          "list",
          "paragraph",
          "reference",
          "table"
        ]
      },
      inline: {
        rules: [
          // "autolink",
          "backticks",
          "emphasis",
          "entity",
          "escape",
          // "html_inline",
          "image", // TODO: remove
          "link",
          "newline",
          "strikethrough",
          "text"
        ]
      }
    }
  });

  // open links in new tab/window
  // https://github.com/markdown-it/markdown-it/blob/master/docs/architecture.md#renderer
  const defaultRender =
    md.renderer.rules.link_open ||
    function(tokens, idx, options, env, self) {
      return self.renderToken(tokens, idx, options);
    };
  md.renderer.rules.link_open = function(tokens, idx, options, env, self) {
    // If you are sure other plugins can't add `target` - drop check below
    const aIndex = tokens[idx].attrIndex("target");
    if (aIndex < 0) {
      tokens[idx].attrPush(["target", "_blank"]); // add new attribute
    } else {
      tokens[idx].attrs[aIndex][1] = "_blank"; // replace value of existing attr
    }
    // pass token to default renderer.
    return defaultRender(tokens, idx, options, env, self);
  };

  // add tag rule to markdown-it
  md.inline.ruler.push("tag", tagRule);
  md.renderer.rules.tag = tagRender;

  // add video rule to markdown-it
  md.inline.ruler.before("escape", "video", videoRule);
  md.renderer.rules.video = videoRender;

  // enable emoji
  const options = { enabled: Object.keys(emojis) };
  md.use(MarkdownItEmoji, options);
  md.renderer.rules.emoji = function(token, idx) {
    const { markup } = token[idx];
    return `<img src="${emojis[markup]}" alt=":${markup}:" class="emoji">`;
  };

  // inject
  inject("md", md);
};
