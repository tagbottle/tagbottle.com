import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/messaging";
import "firebase/storage";
import configs from "./firebaseConfig.json";

export default async ({ store }, inject) => {
  // init
  if (!firebase.apps.length) {
    firebase.initializeApp(configs[process.env.deployTarget].config);
    firebase.firestore().settings({
      timestampsInSnapshots: true
    });
  }

  // messages
  if (process.client) {
    try {
      firebase
        .messaging()
        .usePublicVapidKey(configs[process.env.deployTarget].fcmKey);
      firebase.messaging().onMessage(function(payload) {
        store.dispatch("notification/add", {
          notification: payload.data
        });
      });
    } catch (e) {
      if (!localStorage.getItem("alreadyFcmFailed")) {
        alert(
          "FCM初期化に失敗しました。このブラウザーはプッシュ通知を受け取ることができません :("
        );
        localStorage.setItem("alreadyFcmFailed", 1);
      }
    }
  }

  // offline data
  if (process.client) {
    try {
      // offline data
      // https://firebase.google.com/docs/firestore/manage-data/enable-offline?authuser=0
      firebase.firestore().enablePersistence();
    } catch (e) {
      if (e.code === "failed-precondition") {
        // Multiple tabs open, persistence can only be enabled
        // in one tab at a a time.
        console.error("offline data error: failed-precondition");
      } else if (e.code === "unimplemented") {
        // The current browser does not support all of the
        // features required to enable persistence
        console.error("offline data error: unimplemented");
      }
    }
  }

  // auth
  if (process.client) {
    firebase.auth().onAuthStateChanged(
      async user => {
        await store.dispatch("app/setCurrentUser", { user });
        if (user) {
          store.dispatch("auth/login");
        } else {
          store.dispatch("auth/logout");
        }
      },
      error => {
        console.error("firebase.auth().onAuthStateChanged error", error);
        if (process.client) {
          alert(error);
        }
        store.dispatch("auth/logout");
        store.dispatch("app/setCurrentUser", { user: null });
      }
    );
  }

  inject("firebase", firebase);
  store.commit("app/FIREBASE_INITIALIZED");
};
