// video embed rule
import urlParser from "js-video-url-parser";

export const videoRule = state => {
  const { src } = state;
  // TODO: title parse
  const pattern = /!\[(.*?)\]\(\s*([^\s]*?)\s*\)/;
  const nicovideoPattern = /^https?:\/\/((www|sp)\.nicovideo\.jp\/watch|nico\.ms)\/((sm|nm|so)?\d+)/;

  const matched = src.match(pattern);
  if (!matched) {
    return false;
  }

  // check if the link is video
  const parsed = urlParser.parse(matched[2]);
  const nicovideoParsed = nicovideoPattern.exec(matched[2]);

  function createEmbedIFrame(id, url) {
    state.push("video", "iframe", 0).attrs = [
      ["id", id],
      ["src", url],
      ["width", 640],
      ["height", 360]
    ];
  }

  if (parsed) {
    // create embed link
    const embed = urlParser.create({
      videoInfo: parsed,
      format: "embed"
    });
    createEmbedIFrame(parsed.id, embed);
  } else if (nicovideoParsed) {
    const videoId = nicovideoParsed[3];
    createEmbedIFrame(
      "nicovideo_" + videoId,
      "https://embed.nicovideo.jp/watch/" + videoId
    );
  } else {
    // return false if ![]() is used incorrectly
    return false;
  }

  // seek position
  state.pos += matched[0].length;

  return true;
};

// video embed renderer
export const videoRender = (tokens, idx, options, env, slf) => {
  const token = tokens[idx];
  return (
    '<div class="videoWrapper"><iframe' + slf.renderAttrs(token) + "/></div>"
  );
};
