// tag rule
export const tagRule = state => {
  const pattern = /#(\S+)#/;
  const { pos, src } = state;

  if (src.charCodeAt(pos) !== 0x23 /* # */) {
    return false;
  }

  const matched = src.slice(pos).match(pattern);
  if (!matched) {
    return false;
  }

  // create tag link
  // treat hash as slash
  const themes = matched[0]
    .replace(/^#+|#+$/g, "") // strip # on head and tail
    .replace(/[.#]+/g, "/") // convert # and . to slash
    .split(/\/+/);
  const url = "/chat?themes=" + themes.join("&themes=");

  // update state
  state.push("link_open", "a", 1).attrs = [["href", url]];
  state.push("tag", "", 0).content = matched[0];
  state.push("link_close", "a", -1);

  // seek position
  state.pos += matched[0].length;

  return true;
};

// tag renderer
export const tagRender = (token, idx) => {
  return token[idx].content;
};
