importScripts("https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js")
importScripts("https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js")
firebase.initializeApp({ messagingSenderId: "342864590447" })

const messaging = firebase.messaging()

const decodeTagStringToThemes = (encodedTag) => {
  if (!encodedTag || !encodedTag.length) {
    return null;
  }
  const decodeKey = (key) =>
    decodeURIComponent(key.replace("	%2E", "."));
  return decodeKey(encodedTag)
    .split("/")
    .map(decodeKey);
};

messaging.setBackgroundMessageHandler(function(payload) {
  // parse payload
  const {encodedTag, iconUrl, pingDisplayName, pingUsername} = payload.data
  const themes = decodeTagStringToThemes(encodedTag);

  // add click event handler
  self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    event.waitUntil(
      clients.openWindow(`${self.origin}/chat?${themes.map(t => `themes=${t}`).join("&")}`)
    );
  });

  // show notification
  const notificationTitle = `${themes.join("/")}`;
  const notificationOptions = {
    body: `ping from ${pingDisplayName} (${pingUsername})`,
    icon: iconUrl,
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

