export default ({ store, route, redirect, app }) => {
  // no redirect if route.name is blank (z.B. assets files have no names)
  if (!route.name) {
    return;
  }

  // no redirect if target page is already selected
  const target = "index";
  if (route.name === target) {
    return;
  }

  // no redirect if not needed
  if (!store.getters["auth/loginRedirectNeeded"]) {
    return;
  }

  // redirect
  const rootPath = app.localePath({ name: "index" });
  store.dispatch("auth/logout");
  redirect(rootPath);
};
