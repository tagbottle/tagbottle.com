---
meta:
  - name: description
    content: タグボトルが利用するサービスとライブラリーについて
---

# 依存サービスとライブラリー

タグボトルは既存のリソースを効率よく利用し、開発・運用コストを低減しています。ここでは、それらに関連する開発上の注意点を記載します。

## firebase

* [公式サイト](https://firebase.google.com/)
* [ドキュメント](https://firebase.google.com/docs/)
  
### バージョンアップデート

firebaseのバージョンを更新するときには、以下の点に注意する必要があります。

* `hosting/package.json` を変更した場合、 `hosting/static/firebase-messaging-sw.js` 冒頭のインポート処理も同時に更新する
