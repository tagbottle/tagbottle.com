---
meta:
  - name: description
    content: FCMによる通知サービスの実装について
---

# FCM

新規メッセージの投稿通知など、クライアントへの通知には[FCM](https://firebase.google.com/docs/cloud-messaging/?hl=ja)が利用されています。

## 関連ファイル

### hosting/static/firebase-messaging-sw.js

* アプリケーションのバックグラウンド時に動作
* 配信されたデータをIndexedDBに保存し、同時にユーザーへの通知を行う

### hosting/plugins/fcm.ts

* アプリケーションのフォアグラウンド時に動作
* 配信されたデータをvuex storeに反映・保存する

### hosting/store/notification.ts

* アプリケーション起動時に動作
* IndexedDBを準備し、バックグラウンドからフォアグラウンドへ切り替わった時に動作するイベントを設定する
* 上記イベントがIndexedDBに保存されたデータをvuex storeに反映・保存する

## troubleshooting

### なぜか動かない

* クライアントごと（各ブラウザーや端末など）に固有の通知用トークンが正しく登録されていない可能性がある。再登録で回復することがある
* firebaseのバージョン更新時、サービスワーカーの依存ライブラリと矛盾が生じて壊れることがある。`5.3.0` などと記載された箇所を更新する。

  ```js
  importScripts("https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js")
  importScripts("https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js")
  ```
