---
meta:
  - name: description
    content: Firebase Storage
---

# Firebase Storage

## signedUrl

```
SigningError: Permission iam.serviceAccounts.signBlob is required to perform this operation on service account 
```

* service account認証情報を渡す必要がある
* ドキュメントには `serviceAccount = require('./serviceAccount.json');` と書いてあるが、これは「そういうファイルを置けば動く」という意味で、実際にはそんなファイルは存在しない？
* コンソールから認証用JSONを作成し、環境変数を経由して渡すとよさそう

```bash
firebase functions:config:set --project "$DEV_PROJECT_ID" service-account.json="{\"type\": \"service_account\",\"project_id\": ...}";
```
