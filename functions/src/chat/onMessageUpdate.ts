import "fp-ts";

import { admin, firebaseAdmin, functions } from "../common";

import saveMessageEditLog, {
  IProps
} from "tomato-puree/admin/chat/saveMessageEditLog";
import * as DocTypes from "tomato-puree/admin/firestoreDocType";
import { RefGenerator } from "tomato-puree/admin/ref";

export const onMessageUpdate = functions.firestore
  .document("messages/{messageId}")
  .onUpdate(async (change, context) => {
    // get param from snapshot
    const messageDoc = change.before.data() as DocTypes.IMessageDocument;
    const messageId: string = context.params.messageId;

    // create refs
    const db = admin.firestore();
    const refGenerator = new RefGenerator(db);
    const messageRef = refGenerator.message(messageId);

    // call function
    const props: IProps = {
      messageDoc,
      messageRef,
      timestamp: firebaseAdmin.firestore.FieldValue.serverTimestamp()
    };
    const result = await saveMessageEditLog(db, props);

    // output error
    if (result.isLeft()) {
      console.log("props", props);
      console.error(result.value);
    }
  });

export default onMessageUpdate;
