import { admin, functions } from "../common";

import { DocumentReference } from "@google-cloud/firestore";
import rebottle from "tomato-puree/admin/chat/rebottle";

export interface IProps {
  bottleId: string;
}

export const onCall = functions.https.onCall(async (data: IProps, context) => {
  if (!context.auth) {
    throw new functions.https.HttpsError("unauthenticated", "auth required");
  }

  return await rebottle(admin.firestore(), { bottleId: data.bottleId });
});
