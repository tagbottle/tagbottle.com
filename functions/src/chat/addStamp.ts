import { admin, functions } from "../common";

import { DocumentReference } from "@google-cloud/firestore";
import { CallableContext } from "firebase-functions/lib/providers/https";
import DocTypes from "tomato-puree/admin/firestoreDocType";
import validate from "validate.js";

export interface IProps {
  messageId: string;
  stamp: string;
  stampUrl: string;
}

export const onCall = functions.https.onCall(
  async (data: IProps, context): Promise<IProps> => {
    await addStamp(data, context);
    return data;
  }
);

export interface IResult {
  stampRef: DocumentReference;
  data: IProps;
}

export const addStamp = async (
  data: IProps,
  context: CallableContext
): Promise<IResult> => {
  if (!context.auth) {
    throw new functions.https.HttpsError("unauthenticated", "auth required");
  }

  // check argument
  const constraints = {
    messageId: {
      length: {
        minimum: 1
      },
      presence: true
    },
    stamp: {
      length: {
        minimum: 1
      },
      presence: true
    },
    stampUrl: {
      presence: true,
      url: {
        schemes: ["https"]
      }
    }
  };
  const invalidArguments = validate(data, constraints);
  if (invalidArguments) {
    throw new functions.https.HttpsError(
      "invalid-argument",
      `invalid arguments: ${data}`
    );
  }

  const { messageId, stamp, stampUrl } = data;
  const messageDoc = await admin
    .firestore()
    .collection("messages")
    .doc(messageId)
    .get();
  const { bottle } = messageDoc.data()!;

  // add stamp to `/stamps/{bottleId}` on firestore
  const uid = context.auth.uid;
  const bottleId = bottle.path.split("/")[1];
  const stampRef = await admin
    .firestore()
    .collection("stamps")
    .doc(bottleId);
  await stampRef.set(
    { [messageId]: { [stamp]: { [uid]: true } } },
    { merge: true }
  );

  return { stampRef, data: { messageId, stamp, stampUrl } };
};
