import ping, {
  IProps as PingProps,
  ISuccessPayload
} from "tomato-puree/admin/messaging/ping";
import { RefGenerator } from "tomato-puree/admin/ref";
import { firestore, functions, messaging } from "../common";

export interface IProps {
  themes: string[];
  targetUserId: string;
}

export const sendPing = functions.https.onCall(
  async (props: IProps, context): Promise<ISuccessPayload> => {
    // create refs
    const refGenerator = new RefGenerator(firestore);
    const uid = context.auth!.uid;
    const pingUserRef = refGenerator.user(uid);
    const targetUserRef = refGenerator.user(props.targetUserId);

    // send ping
    const pingProps: PingProps = {
      pingUserRef,
      targetUserRef,
      themes: props.themes
    };
    const result = await ping(firestore, messaging, pingProps);

    if (result.isLeft()) {
      const error = result.value;
      console.log("props", props);
      console.error(result.value);
      throw new functions.https.HttpsError("invalid-argument", error.message);
    }

    return result.value;
  }
);

export default ping;
